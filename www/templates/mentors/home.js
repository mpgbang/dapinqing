this_page = function() {
    return {
        get_data: function() {
            error_msg = $.i18n._('Unable to load mentors data.');

            // Get data from API
            if ($.core.qs('q') !== null) {
                // Get search results
                startrecord = (typeof($.core.qs('startrecord')) != 'undefined' && parseInt($.core.qs('startrecord')) ? parseInt($.core.qs('startrecord')) : 1);
                data = $.pages['mentors.home'].get_search_results(startrecord);
            }else{
                // Load landing page data
                data = {is_search: false};
                data.mentors = $.core.get_from_api('multi/?requests={"mentors/popular": {"as": "popular"}, "mentors/recommended": {"as": "recommended"}, "mentors/new": {"as": "new"}}', error_msg);
            }

            return data;
        },

        get_search_results: function(startrecord, qs) {
            // Derive startrecord
            numrecords = 10;
            startrecord = (typeof(startrecord) != 'undefined' && parseInt(startrecord) ? parseInt(startrecord) : 1);

            // Load search results data
            qs = (typeof(qs) != 'undefined' ? qs : null);
            if (typeof(qs) == 'string')
                try {
                    qs = JSON.parse('{"' + decodeURI(qs).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}');
                }catch(err) {
                    qs = null;
                }
            if (!qs)
                qs = $.core.qs();

            delete qs.startrecord;
            data = {is_search: true, post: qs, qs: $.param(qs), nextrecord: (startrecord+numrecords)};
            data.mentors = $.core.get_from_api('mentors/?'+$.param($.core.qs())+'&numrecords='+numrecords+'&startrecord='+startrecord, null, function(){}, {return_meta: true});

            return data;
        },

        after_load: function() {
            $.core.flipsnap('#mentors-popular');
            $.core.flipsnap('#mentors-recommended');
            $.core.flipsnap('#mentors-new');
        },

        append_results: function(qs, startrecord) {
            $('#ajax_loader').show();
            $('#mentors .show-more a').addClass('not-active');

            $.search_results = $.pages['mentors.home'].get_search_results(startrecord, qs);

            $.ajax({
                url: $.handlebars_templates_base_path+'mentors/item.handlebars.html',
                cache: false,
                success: function(response) {
                    // Compile and register partial
                    template = Handlebars.compile(response);
                    Handlebars.registerPartial('mentor', response);

                    $.ajax({
                        url: $.handlebars_templates_base_path+'mentors/list.handlebars.html',
                        cache: false,
                        success: function(response) {
                            $('#ajax_loader').hide();
                            $('#mentors .show-more a').removeClass('not-active');
                            // Load page
                            $('#mentors .mentors > div').load_template_manually('mentors/list.handlebars.html', $.search_results, function() {
                                // Update Show more link
                                if ($.search_results.mentors.meta.NumRecordsAvailable <= $.search_results.mentors.meta.EndRecord) {
                                    $('#mentors .show-more a').hide();
                                }else{
                                    $('#mentors .show-more a').attr('href', '#mentors?'+$.search_results.qs+'&append_results=1&startrecord='+$.search_results.nextrecord);
                                    $('#mentors .show-more a').attr('onclick', "$.pages['mentors.home'].append_results('"+$.search_results.qs+"', "+$.search_results.nextrecord+"); return false;");
                                }
                            }, true);
                        }
                    });
                }
            });
        }
    };
};