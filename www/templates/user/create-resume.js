this_page = function() {
    return {
        get_data: function() {
            error_msg = $.i18n._('Unable to load your résumé data.');

            // Get user data
            data = $.core.get_from_api('multi/?requests={"users/me?with=resume_information": {"as": "me"}}', error_msg);

            if (!$.isEmptyObject(data)) {
                // Require student
                if (!data.me.is_student) {
                    $.core.show_error($.i18n._('You must have an employee account to view that screen.'));

                    window.setTimeout("$.mobile.navigate('#my-account')", 1500);

                    return false;
                }

                $.my_data = data.me;

                return data;
            }else
                $.mobile.navigate('#home');
        },

        after_load: function() {
            // Show back button
            $.core.show_back_arrow();

            // fix footer z-index
            $('.dropdown').on('show.bs.dropdown', function () {
                $(this).find('ul').addClass('position-relative');
            });
            $('.dropdown').on('hide.bs.dropdown', function () {
                $(this).find('ul').removeClass('position-relative');
            });

        },

        before_submit: function(data) {
            // Show loading indicator
            $.core.show_loading();

            return data;
        },

        update_cb: function(response) {
            // Hide loading indicator
            $.core.hide_loading();

            data = $.core.parse_api_response(response);
            if (!data)
                return $.core.api_error(response, $.i18n._('Unable to create your new résumés.')+' '+$.i18n._('Please try again.'));

            $.core.show_success($.i18n._('Your new résumés have been created.'));

            // Update DOM
            $('#create-resume-form').hide();
            $('#create-resume-review').show();
            $('#create-resume-review input[name="resume_en_uri"]').val(data.resume_en_uri);
            $('#create-resume-review input[name="resume_zh_uri"]').val(data.resume_zh_uri);
            $('#review-resume-en-link').attr('href', data.resume_en_uri).click(function() {
                window.open(data.resume_en_uri, '_blank', 'location=no,enableViewportScale=yes'); return false;
            });
            $('#review-resume-zh-link').attr('href', data.resume_zh_uri).click(function() {
                window.open(data.resume_zh_uri, '_blank', 'location=no,enableViewportScale=yes'); return false;
            });
            $(window).scrollTop(0);
        },

        before_accept: function(data) {
            $('#create-resume-form textarea').each(function(i, elem) {
                data['resume_'+$(elem).attr('name')] = $(elem).val();
            });

            return data;
        },

        accept_resumes_cb: function(response) {
            data = $.core.parse_api_response(response);
            if (!data)
                return $.core.api_error(response, $.i18n._('Unable to save your new résumés.')+' '+$.i18n._('Please try again.'));

            $.core.show_success($.i18n._('Your new résumés have been saved.'));

            window.setTimeout("$.mobile.navigate('#my-resumes')", 1500);
        },

        reject_resumes: function(response) {
            $.core.delete_from_api('resumes/'+$('#create-resume-review input[name="resume_en_uri"]').val().replace(/^.*\/([^\/]*)$/, '$1'));
            $.core.delete_from_api('resumes/'+$('#create-resume-review input[name="resume_zh_uri"]').val().replace(/^.*\/([^\/]*)$/, '$1'), {}, function(response) {
                // Go back to edit mode
                $('#create-resume-review').hide();
                $('#create-resume-form').show();
                $(window).scrollTop(0);
            });

            return false;
        }
    };
};

