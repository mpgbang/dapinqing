this_page = function() {
    return {
        get_data: function() {
            data = {};
            error_msg = $.i18n._('Unable to load candidate.');

            // Get user data
            id = $.core.qs('candidate_id');
            if (id)
                data = $.core.get_from_api('multi/?requests={"students/'+id+'": {"as": "candidate"}, "dapinqing-interview-price": {"as": "price"}}', error_msg);
            else
                $.core.show_error(error_msg);

            if (!$.isEmptyObject(data) && !$.isEmptyObject(data.price)) {
                $.price_credits = data.price.price_credits;

                return data;
            }else{
                $.mobile.navigate('#candidates');
            }
        },

        after_load: function() {
            // Show back button
            $.core.show_back_arrow();

            // Set default date/time
            tomorrow = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
            tomorrow_val = tomorrow.getFullYear() + '-' + ('0' + (tomorrow.getMonth()+1)).slice(-2) + '-' + ('0' + tomorrow.getDate()).slice(-2);
            $('#request-interview #start_date, #request-interview #end_date').val(tomorrow_val);
            $('#request-interview #start_time').val('10:00');
            $('#request-interview #end_time').val('10:30');

            // Update total price
            $.pages['chat.request'].update_total_price();
            $('#request-interview #start_date, #request-interview #end_date, #request-interview #start_time, #request-interview #end_time').blur(function() {
                $.pages['chat.request'].update_total_price();
            });
        },

        update_total_price: function() {
            start_unix = new Date($('#request-interview #start_date').val() +' '+ $('#request-interview #start_time').val()).getTime();
            end_unix = new Date($('#request-interview #end_date').val() +' '+ $('#request-interview #end_time').val()).getTime();
            duration = end_unix - start_unix;

            if (duration <= 0) {
                $.core.show_error($.i18n._('End time must be after Start time.'));

                total_price = 'n/a';
            }else{
                $.core.hide_messages();

                // Calculate price
                if (duration > 0 && (duration / 1000 / 60) % 30 == 0)
                    total_price = ((duration / 1000 / 60 / 30) * $.price_credits) + ' credits';
                else{
                    $.core.show_error($.i18n._('Please select time in increments of 30 minutes.'));

                    total_price = 'n/a';
                }
            }

            // Update display
            $('#request-interview #total_price').text(total_price);
        },

        after_submit: function(response) {
            data = $.core.parse_api_response(response);
            if (!data)
                return $.core.api_error(response, $.i18n._('Unable to save your request.')+' '+$.i18n._('Please try again.'));

            success = false;
            if (typeof(data.inserts) != 'undefined')
                if (data.inserts.length)
                    success = true;

            if (success) {
                $.core.show_success($.i18n._('Your request has been sent.'));

                // Go back
                window.setTimeout("$.core.history.go(-1);", 1500);
            }else
                $.core.show_error($.i18n._('Oops!')+' '+$.i18n._('Unable to send your request.'));
        }
    };
};