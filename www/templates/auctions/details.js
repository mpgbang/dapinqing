this_page = function() {
    return {
        get_data: function() {
            data = {};
            error_msg = $.i18n._('Unable to load auction.');

            // Get mentor data
            id = $.core.qs('id');
            if (id) {
                data.auction = $.core.get_from_api('hiring-pools/'+id+'/?with=me,users,offers', error_msg);

                if (!$.isEmptyObject(data)) {
                    if (data.auction.me.is_student && data.auction.me.hiring_pool_joined_at != null) {
                        data.is_joined = true;
                    }else{
                        data.is_joined = false;
                    }
                }else{
                    $.mobile.navigate('#home');
                }
            }else{
                $.core.show_error(error_msg);
            }

            if (!$.isEmptyObject(data)) {
                $.auction_data = data;
                return data;
            }else{
                $.mobile.navigate('#home');
            }
        },

        after_load: function() {
            // Show back button
            $.core.show_back_arrow();

            $.core.flipsnap('#candidates-top');
            $.core.flipsnap('#auction-offers');
        },

        after_submit: function(response) {
            data = $.core.parse_api_response(response);
            if (!data)
                return $.core.api_error(response, $.i18n._('Unable to join this auction')+' '+$.i18n._('Please try again.'));

            // Success
            $.core.show_success($.i18n._('You have joined this auction.'));
            window.setTimeout("$.core.reload();", 1500);
        },

        check_offer: function(offer_id, status) {
            if (status){
                action = 'accept';
            } else {
                action = 'deny';
            }

            if (action == 'accept' || action == 'deny') {
                success = false;
                $.core.put_to_api('hiring-pools-offers/'+offer_id+'/?'+action, {}, null, function(response) {
                    try {
                        response = $.parseJSON(response.responseText);

                        if (typeof(response.updates) != 'undefined')
                            if (response.updates.length) {
                                success = true;

                                response = response.updates[0];
                            }
                    }catch (e) {}

                    if (success) {
                        $.core.show_success($.i18n._('Offer')+' '+(action == 'accept' ? $.i18n._('accepted') : $.i18n._('denied'))+'.');

                        window.setTimeout("$.core.reload();", 1500);
                    }else
                        $.core.show_error($.i18n._('Oops!')+' '+$.i18n._('Unable to update your offer.'));
                });
            } else {
                $.core.show_error($.i18n._('An error has occurred.'));
            }
        }

    };
};