this_page = function() {
    return {
        get_data: function() {
            data = {};
            error_msg = $.i18n._('Unable to load auction data.');

            // Get user data
            id = $.core.qs('to_user_id');
            if (id) {
                data = $.core.get_from_api('multi/?requests={"users/me": {"as": "me"}, "users/'+id+'": {"as": "user"}, "wage-ranges": {"as": "wage_ranges"}}', error_msg);
            }else{
                $.core.show_error(error_msg);
            }

            if (!$.isEmptyObject(data)) {
                // Require mentor
                if (!data.me.is_mentor) {
                    $.core.show_error($.i18n._('You must have an employer account to view that screen.'));

                    window.setTimeout("$.mobile.navigate('#home')", 1500);

                    return false;
                }

                data.hiring_pool_id = $.core.qs('hiring_pool_id');
                data.post = $.core.qs();

                return data;
            }else{
                $.core.history.go(-1);
            }
        },

        after_load: function() {
            // Show back button
            $.core.show_back_arrow();
        },

        before_offer: function(data) {
            if (!$('#auction-offer select[name="wage_range_id"]').val()) {
                $.core.show_error($.i18n._('Please select a wage.'));

                return false;
            }
        },

        after_offer: function(response) {
            success = false;
            try {
                response = $.parseJSON(response.responseText);
                success = response.success;
            }catch (e) {}

            if (success) {
                $.core.show_success($.i18n._('Your offer has been sent.'));

                window.setTimeout("$.core.history.go(-1);", 1500);
            } else {
                error_msg = response.error.MoreInfo;
                $.core.show_error($.i18n._('Oops!')+' '+$.i18n._(error_msg));
            }
        }
    };
};