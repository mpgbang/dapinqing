this_page = function() {
    return {
        after_load: function(page_name) {
            // Show appropriate header
            if (page_name.match(/^home/)) {
                $('#subpage-header').hide();
                $('#home-header').show();

                $('.main-nav').removeClass('subpage');
                $('.main-nav').addClass('home');
            }else{
                $('#home-header').hide();
                $('#subpage-header').show();

                $('.main-nav').removeClass('home');
                $('.main-nav').addClass('subpage');
            }

            // Update header & footer
            $.core.update_header(page_name);
            $.core.update_footer(page_name);

            // Hamburger button for mobile nav
            $( '.nav-mobile-btn' ).off('click');
            $( '.nav-mobile-btn' ).click(function() {
                $.pages['global'].toggle_mobile_nav();
            });

            // Close menu when link is clicked
            $('.nav-mobile li a').off('click');
            $('.nav-mobile li a').click(function(e, eventData) {
                $.pages['global'].hide_mobile_nav();
            });

            // Close menu when tap/click outside of menu
            $(document).off('mouseup');
            $(document).mouseup(function (e) {
                var nav_mobile = $(".nav-mobile");
                var nav_mobile_trigger = $(".nav-mobile-btn").find('i');
                if (!nav_mobile.is(e.target)
                    && !nav_mobile_trigger.is(e.target)
                    && nav_mobile.has(e.target).length === 0) // ... nor a descendant of the nav_mobile
                {
                    $.pages['global'].hide_mobile_nav();
                }
            });
        },

        show_mobile_nav: function() {
            if( $('.nav-mobile').is(':visible') )
                return;

            $('.nav-mobile').show().animate({
                width: 160
            });
        },
        hide_mobile_nav: function() {
            if( !$('.nav-mobile').is(':visible') )
                return;

            $('.nav-mobile').animate({
                width: 0
            }, function(e) {
                $(this).hide();
            });
        },
        toggle_mobile_nav: function() {
            if( $('.nav-mobile').is(':visible') ) {
                $.pages['global'].hide_mobile_nav();
            } else {
                $.pages['global'].show_mobile_nav();
            }
        },

        _parse_dropdown_selections: function(selections) {
            selections_str = '';
            $.each(selections, function(k, v) {
                if (k.trim())
                    selections_str += k+': ';

                $.each(v, function(selection, bool) {
                    if (bool)
                        selections_str += selection+', ';
                });
                selections_str = selections_str.replace(/, $/, '');
                selections_str += '; ';
            });
            selections_str = selections_str.replace(/; $/, '');

            return selections_str;
        }
    };
};